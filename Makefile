# NOTE:
#   The command using ${MPICC} was modified to remove -lcr and to add:
#      -Wl,--unresolved-symbols=ignore-all
# NOTE:
# mpicc/gcc complains about only having dynamically linked gethostbyname()
#   One solution is to recompile a glibc with:  --enable-static-nss
#   Luckily, these tests run on a single host, and so we just don't
#   link with gethostbyname, and we use '--unresolved-symbols=all'

# NOTE: Update the following variables for your system
CC=gcc
LD=gcc
MPICC=mpicc
RTLD_PATH=/lib64/ld-2.27.so
# MPI_INCLUDE_PATH=/usr/local/include
MPI_INCLUDE_PATH=/usr/include/x86_64-linux-gnu/mpich

FILE=kernel-loader
KERNEL_LOADER_OBJS=${FILE}.o procmapsutils.o custom-loader.o mmap-wrapper.o sbrk-wrapper.o mpi-lh-if.o mem-restore.o utils.o trampoline_setup.o lower-half-mpi.o
TARGET_OBJS=target.o
TARGET_PRELOAD_LIB_OBJS=upper-half-wrappers.o upper-half-mpi-wrappers.o mem-ckpt.o procmapsutils.o utils.o trampoline_setup.o

CFLAGS=-g3 -O0 -fPIC -I. -I${MPI_INCLUDE_PATH} -c -std=gnu11 -Wall -Werror -DDEBUG_LEVEL=ERROR
CFLAGS_SIMGRID=-g3 -O0 -fPIC -I. -c -std=gnu11 -Wall -Werror -DDEBUG_LEVEL=ERROR
KERNEL_LOADER_CFLAGS=-DSTANDALONE
MPICC_FLAGS=-Wl,-Ttext-segment -Wl,0xF00000

KERNEL_LOADER_BIN=kernel-loader.exe
TARGET_BIN=t.exe
TARGET_PRELOAD_LIB=libuhwrappers.so

SIMGRID_OBJS=${FILE}-simgrid.o procmapsutils.o custom-loader.o mmap-wrapper.o sbrk-wrapper.o mpi-lh-if.o mem-restore.o utils.o trampoline_setup.o lower-half-mpi-simgrid.o
SIMGRID_BIN=kernel-loader-dyn.exe

STATIC_TARGET_BIN=static-t
STATIC_TARGET_BIN_RANK0=${STATIC_TARGET_BIN}-rank0.exe
STATIC_TARGET_BIN_RANK1=${STATIC_TARGET_BIN}-rank1.exe
STATIC_TARGET_BIN_RANK0_LDFLAGS=-static -Wl,-Ttext-segment -Wl,0xE000000
STATIC_TARGET_BIN_RANK1_LDFLAGS=-static -Wl,-Ttext-segment -Wl,0xF000000
STATIC_TARGET_OBJS=${TARGET_OBJS} ${TARGET_PRELOAD_LIB_OBJS}

NO_RANDOMIZE=setarch --addr-no-randomize

run: ${KERNEL_LOADER_BIN} ${STATIC_TARGET_BIN_RANK0} ${STATIC_TARGET_BIN_RANK1}
	# TARGET_EXE=${STATIC_TARGET_BIN} ${NO_RANDOMIZE} mpirun -n 2 ./$< arg1 arg2 arg3
	TARGET_EXE=${STATIC_TARGET_BIN} ${NO_RANDOMIZE} mpirun --allow-run-as-root -n 2 ./$< arg1 arg2 arg3

run0: ${KERNEL_LOADER_BIN} ${STATIC_TARGET_BIN_RANK0}
	TARGET_EXE=${STATIC_TARGET_BIN} ${NO_RANDOMIZE} ./$< arg1 arg2 arg3

gdb: ${KERNEL_LOADER_BIN} ${STATIC_TARGET_BIN_RANK0}
	TARGET_EXE=${STATIC_TARGET_BIN} gdb --args ./$< arg1 arg2 arg3

restart0: ${KERNEL_LOADER_BIN} rank_0_ckpt.img
	TARGET_EXE=${STATIC_TARGET_BIN} ${NO_RANDOMIZE} ./$< --restore ./rank_0_ckpt.img

restart-simgrid0: ${SIMGRID_BIN} rank_0_ckpt.img
	TARGET_EXE=${STATIC_TARGET_BIN} smpirun -gdb -np 1 -platform cluster_backbone.xml kernel-loader-dyn.exe --restore ./rank_0_ckpt.img

restart-simgrid: ${SIMGRID_BIN} ./rank_0_ckpt.img ./rank_1_ckpt.img
	TARGET_EXE=${STATIC_TARGET_BIN} ${NO_RANDOMIZE} smpirun --log=simix.t:debug -np 2 -platform cluster_backbone.xml kernel-loader-dyn.exe --restore ./rank_0_ckpt.img ./rank_1_ckpt.img

.c.o:
	${CC} ${CFLAGS} $< -o $@

${FILE}.o: ${FILE}.c
	${CC} ${CFLAGS} ${KERNEL_LOADER_CFLAGS} $< -o $@

${FILE}-simgrid.o: ${FILE}.c
	smpicc ${CFLAGS_SIMGRID} ${KERNEL_LOADER_CFLAGS} $< -o $@

lower-half-mpi.o: lower-half-mpi.c
	${CC} ${CFLAGS} ${KERNEL_LOADER_CFLAGS} $< -o $@

lower-half-mpi-simgrid.o: lower-half-mpi.c
	smpicc ${CFLAGS_SIMGRID} ${KERNEL_LOADER_CFLAGS} $< -o $@

${TARGET_BIN}: ${TARGET_OBJS}
	${LD} $< -o $@

${STATIC_TARGET_BIN_RANK0}: ${STATIC_TARGET_OBJS}
	${LD} ${STATIC_TARGET_BIN_RANK0_LDFLAGS} $^ -o $@

${STATIC_TARGET_BIN_RANK1}: ${STATIC_TARGET_OBJS}
	${LD} ${STATIC_TARGET_BIN_RANK1_LDFLAGS} $^ -o $@

${TARGET_PRELOAD_LIB}: ${TARGET_PRELOAD_LIB_OBJS}
	${LD} -shared $^ -o $@

# I used:  ${MPICC} -compile_info -static ${MPICC_FLAGS} $^ -o $@ -lpthread
#          to expand 'mpicc ...' into 'gcc ...'
# NOTE:  -link_info also exists 
${KERNEL_LOADER_BIN}: ${KERNEL_LOADER_OBJS}
	# ${MPICC} -static ${MPICC_FLAGS} $^ -o $@ -lpthread
	gcc -Wl,-Bsymbolic-functions -Wl,-z,relro -Wl,-Bsymbolic-functions -Wl,-z,relro -static -Wl,-Bsymbolic-functions -Wl,-z,relro -I/usr/include/mpich -L/usr/lib/x86_64-linux-gnu -lmpich -static -Wl,-Ttext-segment -Wl,0xF00000 kernel-loader.o procmapsutils.o custom-loader.o mmap-wrapper.o sbrk-wrapper.o mpi-lh-if.o mem-restore.o utils.o trampoline_setup.o lower-half-mpi.o -o kernel-loader.exe -lpthread -I/usr/include/mpich -L/usr/lib/x86_64-linux-gnu -lmpich -lbacktrace -lpthread -lrt -Wl,--unresolved-symbols=ignore-all

${SIMGRID_BIN}: ${SIMGRID_OBJS}
	smpicc $^ -o $@

vi vim:
	vim ${FILE}.c

tags:
	gtags .

dist: clean
	(dir=`basename $$PWD` && cd .. && tar zcvf $$dir.tgz $$dir)
	(dir=`basename $$PWD` && ls -l ../$$dir.tgz)

tidy:
	rm -f *.img *.bin *.so smpitmp-*

clean: tidy
	rm -f ${KERNEL_LOADER_OBJS} ${TARGET_OBJS} ${KERNEL_LOADER_BIN} \
	      ${TARGET_BIN} ${TARGET_PRELOAD_LIB_OBJS} ${TARGET_PRELOAD_LIB} \
				${SIMGRID_BIN} ${STATIC_TARGET_BIN_RANK0} ${STATIC_TARGET_BIN_RANK1} \
				${FILE}-simgrid.o lower-half-mpi-simgrid.o \
		  GTAGS GRTAGS GPATH

.PHONY: dist vi vim clean gdb tags tidy restart run run0 enableASLR disableASLR restart-simgrid
